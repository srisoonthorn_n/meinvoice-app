/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Main from './src/Main';
import Add_Invoice from './src/Add_Invoice';
import Invoice from './src/Invoice';
import Router from './src/router/Router';

AppRegistry.registerComponent(appName, () => Router);
