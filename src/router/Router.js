import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Main from '../Main';
import Add_Invoice from '../Add_Invoice';
import Invoice from '../Invoice';

const Tab = createMaterialTopTabNavigator();


const Stack = createStackNavigator();

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }} >
                <Tab.Screen name="Main" component={Main} />
                <Tab.Screen name="Add_Invoice" component={Add_Invoice} />
                <Tab.Screen name="Invoice" component={Invoice} />
            </Stack.Navigator>
        </NavigationContainer>


    )
}