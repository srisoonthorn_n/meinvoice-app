import React from 'react'
import { View, Text, ScrollView, TouchableOpacity, SafeAreaView, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';

const DATA = [
    {
        id: '1',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Tanaka Sakamoto',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '12 May 2020',
    },
    {
        id: '2',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'My Customer Company',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '10 May 2020',
    },
    {
        id: '3',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },
    {
        id: '4',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },
    {
        id: '5',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },
    {
        id: '6',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },
    {
        id: '7',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },
    {
        id: '8',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },

];


export default function Main(props) {
    return (
        <View>

            <View style={{ flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', margin: 10 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ color: '#707070', fontSize: 40, fontFamily: 'Dosis', fontWeight: 'bold' }}>Me</Text>
                        <Text style={{ color: '#9DC6A7', fontSize: 40, fontFamily: 'Dosis', fontWeight: 'bold' }}>Invoice</Text>
                    </View>
                    <Text style={{ color: '#999999', fontSize: 16, fontFamily: 'Kanit' }}>ALL INVOICES</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', margin: 10, marginEnd: 16 }}>
                    <TouchableOpacity style={{ height: 56, width: 96 }} onPress={() => props.navigation.navigate('Add_Invoice')}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', backgroundColor: '#74957C', borderRadius: 24 }}>
                            <View style={{ margin: 10 }}>
                                <Icon name='plus' size={16} color='#FFFFFF' ></Icon>
                            </View>
                            <View style={{ marginEnd: 10 }}>
                                <Text style={{ color: '#FFFFFF', fontWeight: 'bold', fontSize: 16 }}>NEW</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>


            <ScrollView style={{ height: 600, marginTop: 14, marginBottom: 24 }} contentContainerStyle={{ flexGrow: 1 }}>
                <SafeAreaView>
                    <FlatList
                        data={DATA}
                        renderItem={({ item }) => (
                            <TouchableOpacity style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }}
                                onPress={() => props.navigation.navigate('Invoice')}>
                                <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                                    <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                                    </View>

                                </View>

                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <View>
                                        <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>{item.nameCustomer}</Text>
                                    </View>

                                    <View>
                                        <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>{item.DueDate}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>


                        )}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>
                {/* <TouchableOpacity style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }} 
                onPress={() => props.navigation.navigate('Invoice')}>
                    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                        <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                        </View>

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>MR.Tanaka Sakamoto</Text>
                        </View>

                        <View>
                            <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>2 April 2020</Text>
                        </View>
                    </View>
                </TouchableOpacity>


                <View style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }}>
                    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                        <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                        </View>

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>MR.Tanaka Sakamoto</Text>
                        </View>

                        <View>
                            <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>2 April 2020</Text>
                        </View>
                    </View>
                </View>

                <View style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }}>
                    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                        <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                        </View>

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>MR.Tanaka Sakamoto</Text>
                        </View>

                        <View>
                            <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>2 April 2020</Text>
                        </View>
                    </View>
                </View>

                <View style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }}>
                    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                        <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                        </View>

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>MR.Tanaka Sakamoto</Text>
                        </View>

                        <View>
                            <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>2 April 2020</Text>
                        </View>
                    </View>
                </View>

                <View style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }}>
                    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                        <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                        </View>

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>MR.Tanaka Sakamoto</Text>
                        </View>

                        <View>
                            <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>2 April 2020</Text>
                        </View>
                    </View>
                </View>

                <View style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }}>
                    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                        <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                        </View>

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>MR.Tanaka Sakamoto</Text>
                        </View>

                        <View>
                            <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>2 April 2020</Text>
                        </View>
                    </View>
                </View>

                <View style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }}>
                    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                        <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                        </View>

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>MR.Tanaka Sakamoto</Text>
                        </View>

                        <View>
                            <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>2 April 2020</Text>
                        </View>
                    </View>
                </View>

                <View style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }}>
                    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                        <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                        </View>

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>MR.Tanaka Sakamoto</Text>
                        </View>

                        <View>
                            <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>2 April 2020</Text>
                        </View>
                    </View>
                </View>

                <View style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }}>
                    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                        <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                        </View>

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>MR.Tanaka Sakamoto</Text>
                        </View>

                        <View>
                            <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>2 April 2020</Text>
                        </View>
                    </View>
                </View>

                <View style={{ height: 72, marginBottom: 12, marginEnd: 16, marginStart: 16, backgroundColor: '#00000029', borderRadius: 5, flexDirection: 'row' }}>
                    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', marginEnd: 30, marginStart: 20 }}>
                        <View style={{ borderRadius: 5, backgroundColor: '#9DC6A7', height: 40, width: 40, justifyContent: 'center', alignItems: 'center' }}>
                            <Icon name='file-alt' size={24} color='#FFFFFF' ></Icon>
                        </View>

                    </View>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View>
                            <Text style={{ color: '#000000', fontSize: 14, fontFamily: 'Kanit' }}>MR.Tanaka Sakamoto</Text>
                        </View>

                        <View>
                            <Text style={{ color: '#999999', fontSize: 12, fontFamily: 'Kanit' }}>2 April 2020</Text>
                        </View>
                    </View>
                </View> */}

            </ScrollView>



        </View>
    )
} 