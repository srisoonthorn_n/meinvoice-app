import React from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';


const DATA = [
    {
        id: '1',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 Sodales Av.',

        nameCustomer: 'MR.Tanaka Sakamoto',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: 'Ap #651-8679 Sodales Av. Tamuning PA 10855',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '12 May 2020',
        Description : 'Coca Cola(Bottle)',
        Qty :20,
        price : 14,
        total : 280,
        subTotal : 280,
        grandTotal : 299.6,

        note : ' Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC.This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.'},
    {
        id: '2',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'My Customer Company',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '10 May 2020',
    },
    {
        id: '3',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },
    {
        id: '4',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },
    {
        id: '5',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },
    {
        id: '6',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },
    {
        id: '7',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },
    {
        id: '8',
        nameCompany: 'Dragon Enterprise',
        texIdCompany: '0105535137307',
        emailCompany: 'contact@dragon.com',
        phoneNumberCompany: '089-724-2744',
        addressProfile: 'Ap #651-8679 soDales Av.',

        nameCustomer: 'MR.Sakuke Ujiha',
        texIdCustomer: '0304532136709',
        emailCustomer: 'contact@mycustomer.com',
        addressCustomer: '090-112-1567',

        invoiceID: 'ID-2-27',
        IssueDate: '24 Mar 2020',
        DueDate: '9 May 2020',
    },

];

export default function Invoice(props) {
    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#F8F8F7' }} contentContainerStyle={{ flexGrow: 1 }}>

            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 40, marginStart: 24 }}>

                <View style={{ flex: 0.5 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Main')}>
                        <Icon name='arrow-left' size={16} color='#000000' ></Icon>
                    </TouchableOpacity>
                </View>



                <View style={{ flex: 1 }}>
                    <Text style={{ fontFamily: 'Kanit', fontSize: 16, color: '#555555' }}>VIEW INVOICE</Text>
                </View>
            </View>

            <View style={{ flex: 1, backgroundColor: '#FFFFFF', marginTop: 24, marginBottom: 24, marginStart: 16, marginEnd: 24, borderRadius: 8, shadowOffset: { width: 0, height: 10 }, shadowColor: '#00000029', shadowOpacity: 6, elevation: 5 }}>

                <View style={{ margin: 32 }}>

                    <View style={{ justifyContent: 'center', alignItems: 'center', }}>

                        <View style={{ backgroundColor: '#000000', height: 59, width: 79 }}>
                        </View>

                        <View style={{ marginTop: 12 }}>
                            <Text style={{ fontSize: 16, fontFamily: 'Sarabun', fontWeight: 'bold', color: '#424242' }}>{DATA[0].nameCompany}</Text>
                        </View>

                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: '#555555', fontFamily: 'Sarabun', fontSize: 12 }}>
                                {DATA[0].addressProfile}
                            </Text>

                            <Text style={{ color: '#555555', fontFamily: 'Sarabun', fontSize: 12 }}>
                                Tamuning PA 10855
                        </Text>
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 12 }}>
                        <View style={{ flex: 1, alignItems: 'flex-start' }}>
                            <Text style={{ color: '#555555', fontWeight: 'bold', fontSize: 16, fontFamily: 'Sarabun' }}>INVOICE</Text>

                            <View style={{ flexDirection: 'row', marginTop: 3 }}>
                                <View style={{ marginEnd: 5 }}>
                                    <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun', fontWeight: 'bold' }}>
                                        No.
                                    </Text>
                                </View>

                                <View>
                                    <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun' }}>
                                        {DATA[0].invoiceID}
                                    </Text>
                                </View>

                            </View>

                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginEnd: 5 }}>
                                    <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun', fontWeight: 'bold' }}>
                                        Issue :
                                    </Text>
                                </View>

                                <View>
                                    <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun' }}>
                                        {DATA[0].IssueDate}
                                    </Text>
                                </View>

                            </View>

                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginEnd: 5 }}>
                                    <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun', fontWeight: 'bold' }}>
                                        Due :
                                    </Text>
                                </View>

                                <View>
                                    <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun' }}>
                                        {DATA[0].DueDate}
                                    </Text>
                                </View>

                            </View>
                        </View>

                        <View style={{ flex: 1, alignItems: 'flex-end' }}>

                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginEnd: 5 }}>
                                    <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun', fontWeight: 'bold' }}>
                                        TAX ID
                                    </Text>
                                </View>

                                <View>
                                    <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun' }}>
                                        {DATA[0].texIdCompany}
                                    </Text>
                                </View>

                            </View>


                            <View>
                                <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun' }}>
                                    {DATA[0].emailCompany}
                                </Text>
                            </View>



                            <View>
                                <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun' }}>
                                    {DATA[0].phoneNumberCompany}
                                </Text>
                            </View>


                        </View>

                    </View>

                    <View style={{ marginTop: 16, marginBottom: 4 }}>
                        <Text style={{ color: '#A7CCB0', fontFamily: 'Sarabun', fontSize: 14, fontWeight: 'bold' }}>BILL TO</Text>

                        <View style={{ marginBottom: 4 }}>
                            <Text style={{ color: '#424242', fontFamily: 'Sarabun', fontSize: 12, fontWeight: 'bold' }}>{DATA[0].nameCustomer}</Text>
                            <Text style={{ fontSize: 12, color: '#555555', fontFamily: 'Sarabun' }}>
                            {DATA[0].addressCustomer}
                            </Text>
                        </View>

                        <View style={{ marginTop: 4 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ marginEnd: 5 }}>
                                    <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun', fontWeight: 'bold' }}>
                                        TAX ID
                                    </Text>
                                </View>
                                <View>
                                    <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun' }}>
                                    {DATA[0].texIdCustomer}
                                    </Text>
                                </View>

                            </View>
                            <View>
                                <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun' }}>
                                {DATA[0].emailCustomer}
                                    </Text>
                            </View>
                            <View>
                                <Text style={{ color: '#424242', fontSize: 12, fontFamily: 'Sarabun' }}>
                                    090-112-1567
                                    </Text>
                            </View>
                        </View>

                    </View>

                    <View style={{ marginTop: 14 }}>
                        <View style={{ flex: 1, height: 32, backgroundColor: '#D1F5D3', borderRadius: 8, flexDirection: 'row', marginBottom: 10 }}>
                            <View style={{ flex: 3, justifyContent: 'center', alignItems: 'flex-start', marginStart: 10 }}>
                                <Text style={{ color: '#333333', fontFamily: 'Sarabun', fontSize: 12, fontWeight: 'bold' }}>Description</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#333333', fontFamily: 'Sarabun', fontSize: 12, fontWeight: 'bold' }}>Qty</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#333333', fontFamily: 'Sarabun', fontSize: 12, fontWeight: 'bold' }}>Price</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginEnd: 10 }}>
                                <Text style={{ color: '#333333', fontFamily: 'Sarabun', fontSize: 12, fontWeight: 'bold' }}>Total</Text>
                            </View>
                        </View>

                        <View style={{ flex: 1, borderRadius: 8, flexDirection: 'row' }}>
                            <View style={{ flex: 3, justifyContent: 'center', alignItems: 'flex-start', marginStart: 10 }}>
                                <Text style={{ color: '#333333', fontFamily: 'Sarabun', fontSize: 12 }}>{DATA[0].Description}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#333333', fontFamily: 'Sarabun', fontSize: 12 }}>{DATA[0].Qty}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#333333', fontFamily: 'Sarabun', fontSize: 12 }}>{DATA[0].price}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginEnd: 10 }}>
                                <Text style={{ color: '#333333', fontFamily: 'Sarabun', fontSize: 12 }}>{DATA[0].total}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{ marginTop: 40, marginStart: 170 }}>
                        <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <View style={{ marginEnd: 10 }}>
                                <Text style={{ fontFamily: 'Sarabun', fontSize: 11 }}>
                                    Sub Total
                                </Text>
                            </View>

                            <View style={{ marginEnd: 10 }}>
                                <Text style={{ fontFamily: 'Sarabun', fontSize: 11, fontWeight: 'bold' }}>
                                {DATA[0].subTotal}
                                </Text>
                            </View>
                        </View>

                        <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <View style={{ marginEnd: 10 }}>
                                <Text style={{ fontFamily: 'Sarabun', fontSize: 11 }}>
                                    Vat(7%)
                                </Text>
                            </View>

                            <View style={{ marginEnd: 10 }}>
                                <Text style={{ fontFamily: 'Sarabun', fontSize: 11, fontWeight: 'bold' }}>
                                    19.6
                                </Text>
                            </View>
                        </View>


                        <View style={{ borderBottomColor: '#A7E8AD', borderBottomWidth: 1, marginTop: 6, marginBottom: 6, flexDirection: 'row' }} />
                        <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <View style={{ marginEnd: 10 }}>
                                <Text style={{ fontFamily: 'Sarabun', fontSize: 11 }}>
                                    Grand Total
                                </Text>
                            </View>

                            <View style={{ marginEnd: 10, alignItems: 'flex-end' }}>
                                <Text style={{ fontFamily: 'Sarabun', fontSize: 11, fontWeight: 'bold' }}>
                                {DATA[0].grandTotal}
                                </Text>
                            </View>
                        </View>
                    </View>

                    <View style={{ marginTop: 24, marginBottom: 8 }}>
                        <Text style={{ color: '#424242', fontWeight: 'bold', fontSize: 14 }}>Note</Text>
                    </View>

                    <View style={{ marginTop: 8, flex: 1 }}>
                        <Text maxLength={1000} style={{ color: '#333333', fontFamily: 'Sarabun', fontSize: 12, width: 300 }}>
                           {DATA[0].note}
                        </Text>
                    </View>
                </View>

            </View>

        </ScrollView>
    )
}