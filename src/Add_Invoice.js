import React, { useState } from 'react'
import { View, Text, TouchableOpacity, ScrollView, TextInput, Modal, SafeAreaView } from 'react-native'
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';

export default function Add_Invoice(props) {

    const [showModalAdd, setShowModalAdd] = useState(false);
    const [showModalEdit, setShowModalEdit] = useState(false);

    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setDate(currentDate);
        setShow(false)
    };

    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };



    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#AAAAAA29' }} contentContainerStyle={{ flexGrow: 1 }} >
            <StatusBar translucent={true} backgroundColor={'transparent'} />

            <View style={{ flexDirection: 'row', marginTop: 36 }}>
                <View style={{ flex: 0, marginStart: 24 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Main')}>
                        <Icon name='arrow-left' size={16} color='#000000' ></Icon>
                    </TouchableOpacity>

                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 16, fontFamily: 'Kanit' }}>CREATE INVOICE </Text>
                </View>
            </View>

            <View style={{ marginTop: 24, marginStart: 24, marginBottom: 8 }}>
                <Text style={{ fontSize: 16, fontFamily: 'Kanit', color: '#555555', fontWeight: 'bold' }}>Company Information</Text>
            </View>

            <View style={{ flex: 1, backgroundColor: '#FFFFFF', borderRadius: 8, marginStart: 16, marginEnd: 16 }}>
                <View style={{ margin: 16 }}>

                    <View style={{ marginBottom: 8 }}>
                        <View style={{ marginBottom: 4 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>Logo</Text>
                        </View>

                        <TouchableOpacity>
                            <View style={{ width: 80, height: 80, backgroundColor: '#FCFCFC', padding: 10, justifyContent: 'center', alignItems: 'center', borderRadius: 8, borderWidth: 1, borderColor: '#EEEEEE' }}>
                                <Icon name='camera-retro' size={32} color='#9DC6A7' ></Icon>
                            </View>
                        </TouchableOpacity>
                    </View>


                    <View style={{ marginBottom: 8 }}>

                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>Name</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput style={{ backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>Tax Id</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput style={{ backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>E-Mail Address</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput style={{ backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>Phone Number</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput style={{ backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>Address</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput
                                maxLength={1000}
                                multiline={true}
                                style={{ height: 88, alignItems: 'flex-start', backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>

                </View>
            </View>

            <View style={{ marginTop: 24, marginStart: 24, marginBottom: 8 }}>
                <Text style={{ fontSize: 16, fontFamily: 'Kanit', color: '#555555', fontWeight: 'bold' }}>Your Customer</Text>
            </View>

            <View style={{ flex: 1, backgroundColor: '#FFFFFF', borderRadius: 8, marginStart: 16, marginEnd: 16, marginBottom: 8 }}>
                <View style={{ margin: 16 }}>

                    <View style={{ marginBottom: 8 }}>

                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>Name</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput style={{ backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>

                    <View style={{ marginBottom: 8 }}>

                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>Tax Id</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput style={{ backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>

                    <View style={{ marginBottom: 8 }}>

                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>E-Mail Address</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput style={{ backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>Address</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput
                                maxLength={1000}
                                multiline={true}
                                style={{ height: 88, alignItems: 'flex-start', backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>
                </View>
            </View>

            <View style={{ marginTop: 16, marginStart: 24, marginBottom: 8 }}>
                <Text style={{ fontSize: 16, fontFamily: 'Kanit', color: '#555555', fontWeight: 'bold' }}>Invoice Information</Text>
            </View>

            <View style={{ flex: 1, backgroundColor: '#FFFFFF', borderRadius: 8, marginStart: 16, marginEnd: 16, marginBottom: 8 }}>
                <View style={{ margin: 16 }}>
                    <View style={{ marginBottom: 8 }}>

                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>No.</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput style={{ backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>

                    <View style={{ marginBottom: 8 }}>

                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>Issue Date</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput onFocus={showDatepicker} value={date} style={{ backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>

                    <View style={{ marginBottom: 8 }}>

                        <View style={{ marginTop: 2 }}>
                            <Text style={{ fontSize: 14, fontFamily: 'Kanit', color: '#AAAAAA' }}>Due Date</Text>
                        </View>

                        <View style={{ marginTop: 2 }}>
                            <TextInput onFocus={showDatepicker} style={{ backgroundColor: '#FCFCFC', borderColor: '#EEEEEE', borderWidth: 1, borderRadius: 8 }} />
                        </View>
                    </View>
                </View>
            </View>

            <View style={{ marginTop: 16, marginStart: 24, marginBottom: 8 }}>
                <Text style={{ fontSize: 16, fontFamily: 'Kanit', color: '#555555', fontWeight: 'bold' }}>Item</Text>
            </View>

            <View style={{ flex: 1, marginBottom: 16, marginStart: 16, marginEnd: 16 }}>
                <View style={{ flex: 1, backgroundColor: '#FFFFFF', marginBottom: 1 }}>
                    <View style={{ height: 48, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                        <View style={{ height: 48, width: 32, backgroundColor: '#CCCCCC', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: '#FFFFFF', fontSize: 16, fontWeight: 'bold' }}>1</Text>
                        </View>

                        <View style={{ flex: 1, marginStart: 10, alignItems: 'flex-start', justifyContent: 'center' }}>
                            <Text style={{ color: '#666666', fontSize: 14 }}>Coca Cola(Bottle)</Text>
                            <Text style={{ color: '#999999', fontSize: 10 }}>Qty : 20 | Unit Price : 14 | Total : 280</Text>
                        </View>

                        <View style={{ flex: 0.5, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <View style={{ margin: 8 }}>
                                <TouchableOpacity onPress={() => setShowModalEdit(true)}>
                                    <Icon name='pen-square' size={24} color='#AAAAAA' ></Icon>
                                </TouchableOpacity>
                            </View>
                            <View style={{ margin: 8, marginEnd: 4 }}>
                                <TouchableOpacity>
                                    <Icon name='trash' size={24} color='#AAAAAA' ></Icon>
                                </TouchableOpacity>

                            </View>

                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, backgroundColor: '#FFFFFF', marginBottom: 1 }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                        <View style={{ height: 48, width: 32, backgroundColor: '#CCCCCC', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: '#FFFFFF', fontSize: 16, fontWeight: 'bold' }}>2</Text>
                        </View>

                        <View style={{ flex: 1, marginStart: 10, alignItems: 'flex-start', justifyContent: 'center' }}>
                            <Text style={{ color: '#666666', fontSize: 14 }}>Coca Cola(Bottle)</Text>
                            <Text style={{ color: '#999999', fontSize: 10 }}>Qty : 20 | Unit Price : 14 | Total : 280</Text>
                        </View>

                        <View style={{ flex: 0.5, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <View style={{ margin: 8 }}>
                                <TouchableOpacity onPress={() => setShowModalEdit(true)}>
                                    <Icon name='pen-square' size={24} color='#AAAAAA' ></Icon>
                                </TouchableOpacity>
                            </View>

                            <View style={{ margin: 8, marginEnd: 4 }}>
                                <TouchableOpacity>
                                    <Icon name='trash' size={24} color='#AAAAAA' ></Icon>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                </View>


                <View style={{ flex: 1, backgroundColor: '#FFFFFF', marginBottom: 1 }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                        <View style={{ height: 48, width: 32, backgroundColor: '#CCCCCC', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: '#FFFFFF', fontSize: 16, fontWeight: 'bold' }}>3</Text>
                        </View>

                        <View style={{ flex: 1, marginStart: 10, alignItems: 'flex-start', justifyContent: 'center' }}>
                            <Text style={{ color: '#666666', fontSize: 14 }}>Coca Cola(Bottle)</Text>
                            <Text style={{ color: '#999999', fontSize: 10 }}>Qty : 20 | Unit Price : 14 | Total : 280</Text>
                        </View>

                        <View style={{ flex: 0.5, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <View style={{ margin: 8 }}>
                                <TouchableOpacity onPress={() => setShowModalEdit(true)}>
                                    <Icon name='pen-square' size={24} color='#AAAAAA' ></Icon>
                                </TouchableOpacity>
                            </View>
                            <View style={{ margin: 8, marginEnd: 4 }}>
                                <TouchableOpacity>
                                    <Icon name='trash' size={24} color='#AAAAAA' ></Icon>
                                </TouchableOpacity>

                            </View>

                        </View>
                    </View>
                </View>

            </View>


            <TouchableOpacity
                onPress={() => setShowModalAdd(true)}
                style={{ flex: 1, borderStyle: 'dashed', borderRadius: 8, borderColor: '#B9CEBE', borderWidth: 1, marginStart: 16, marginEnd: 16, marginBottom: 8 }}>
                <View style={{ flex: 1, height: 40, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                    <View style={{ marginEnd: 4 }}>
                        <Icon name='plus-circle' size={24} color='#9DC6A7' ></Icon>
                    </View>
                    <View style={{ marginStart: 4 }}>
                        <Text style={{ color: '#9DC6A7', fontSize: 16, fontWeight: 'bold' }}>ADD</Text>
                    </View>
                </View>
            </TouchableOpacity>

            <Modal
                transparent={true}
                visible={showModalAdd}>
                <View style={{ backgroundColor: '#000000aa', flex: 1 }}>
                    <View style={{ backgroundColor: '#ffffff', marginLeft: 32, marginRight: 32, marginBottom: 90, marginTop: 90, padding: 24, borderRadius: 8, height: 500 }}>

                        <View style={{ marginBottom: 16, flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ color: '#555555', fontFamily: 'Kanit', fontSize: 16, fontWeight: 'bold' }}>ADD NEW ITEM</Text>
                            </View>

                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => setShowModalAdd(false)}>
                                    <Icon name='times' size={24} color='#000000' ></Icon>
                                </TouchableOpacity>

                            </View>

                        </View>

                        <View style={{ marginBottom: 12 }}>

                            <View style={{ marginBottom: 4 }}>
                                <Text style={{ color: '#AAAAAA', fontFamily: 'Kanit', fontSize: 14, }}>Description</Text>
                            </View>

                            <View>
                                <TextInput
                                    maxLength={1000}
                                    multiline={true}
                                    style={{ height: 80, backgroundColor: '#FCFCFC', borderRadius: 8, borderColor: '#EEEEEE', borderWidth: 1 }} />
                            </View>
                        </View>

                        <View style={{ marginBottom: 12 }}>

                            <View style={{ marginBottom: 4 }}>
                                <Text style={{ color: '#AAAAAA', fontFamily: 'Kanit', fontSize: 14, }}>Qty</Text>
                            </View>

                            <View>
                                <TextInput
                                    keyboardType={'number-pad'}
                                    style={{ backgroundColor: '#FCFCFC', borderRadius: 8, borderColor: '#EEEEEE', borderWidth: 1 }} />
                            </View>
                        </View>

                        <View style={{ marginBottom: 12 }}>

                            <View style={{ marginBottom: 4 }}>
                                <Text style={{ color: '#AAAAAA', fontFamily: 'Kanit', fontSize: 14, }}>Unit Price</Text>
                            </View>

                            <View>
                                <TextInput
                                    keyboardType={'number-pad'}
                                    style={{ backgroundColor: '#FCFCFC', borderRadius: 8, borderColor: '#EEEEEE', borderWidth: 1 }} />
                            </View>
                        </View>

                        <View style={{ marginBottom: 12 }}>

                            <View style={{ marginBottom: 4 }}>
                                <Text style={{ color: '#AAAAAA', fontFamily: 'Kanit', fontSize: 14, }}>Total Price</Text>
                            </View>

                            <View>
                                <Text style={{ color: '#9DC6A7', fontWeight: 'bold', fontSize: 16, fontFamily: 'Kanit' }}>0</Text>
                            </View>
                        </View>

                        <TouchableOpacity
                            onPress={() => setShowModalAdd(false)}
                            style={{ flex: 1, margin: 16, }}>
                            <View style={{ height: 40, justifyContent: 'center', alignItems: 'center', backgroundColor: '#74957C', borderRadius: 24 }}>
                                <Text style={{ color: '#FFFFFF', fontSize: 16, fontWeight: 'bold' }}>ADD</Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                </View>
            </Modal>


            <View style={{ marginTop: 16, marginStart: 24, marginBottom: 8 }}>
                <Text style={{ fontSize: 16, fontFamily: 'Kanit', color: '#555555', fontWeight: 'bold' }}>Note</Text>
            </View>

            <View style={{ flex: 1, backgroundColor: '#FFFFFF', borderRadius: 8, marginStart: 16, marginEnd: 16, marginBottom: 8 }}>
                <View style={{ margin: 16, height: 140 }}>
                    <TextInput
                        maxLength={1000}
                        multiline={true}
                        style={{ backgroundColor: '#FCFCFC', height: 140, borderRadius: 8, borderColor: '#EEEEEE', borderWidth: 1 }} />
                </View>
            </View>

            <TouchableOpacity style={{ flex: 1, margin: 16, marginTop: 16, marginBottom: 16 }}
                onPress={() => props.navigation.navigate('Main')}>
                <View style={{ height: 48, justifyContent: 'center', alignItems: 'center', backgroundColor: '#74957C', borderRadius: 24 }}>
                    <Text style={{ color: '#FFFFFF', fontSize: 16, fontWeight: 'bold' }}>CREATE</Text>
                </View>
            </TouchableOpacity>



            <Modal
                transparent={true}
                visible={showModalEdit}>
                <View style={{ backgroundColor: '#000000aa', flex: 1 }}>
                    <View style={{ backgroundColor: '#ffffff', marginLeft: 32, marginRight: 32, marginBottom: 90, marginTop: 90, padding: 24, borderRadius: 8, height: 500 }}>

                        <View style={{ marginBottom: 16, flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ color: '#555555', fontFamily: 'Kanit', fontSize: 16, fontWeight: 'bold' }}>EDIT ITEM</Text>
                            </View>

                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                <TouchableOpacity onPress={() => setShowModalEdit(false)}>
                                    <Icon name='times' size={24} color='#000000' ></Icon>
                                </TouchableOpacity>

                            </View>

                        </View>

                        <View style={{ marginBottom: 12 }}>

                            <View style={{ marginBottom: 4 }}>
                                <Text style={{ color: '#AAAAAA', fontFamily: 'Kanit', fontSize: 14, }}>Description</Text>
                            </View>

                            <View>
                                <TextInput
                                    maxLength={1000}
                                    multiline={true}
                                    style={{ height: 80, backgroundColor: '#FCFCFC', borderRadius: 8, borderColor: '#EEEEEE', borderWidth: 1 }} />
                            </View>
                        </View>

                        <View style={{ marginBottom: 12 }}>

                            <View style={{ marginBottom: 4 }}>
                                <Text style={{ color: '#AAAAAA', fontFamily: 'Kanit', fontSize: 14, }}>Qty</Text>
                            </View>

                            <View>
                                <TextInput
                                    keyboardType={'number-pad'}
                                    style={{ backgroundColor: '#FCFCFC', borderRadius: 8, borderColor: '#EEEEEE', borderWidth: 1 }} />
                            </View>
                        </View>

                        <View style={{ marginBottom: 12 }}>

                            <View style={{ marginBottom: 4 }}>
                                <Text style={{ color: '#AAAAAA', fontFamily: 'Kanit', fontSize: 14, }}>Unit Price</Text>
                            </View>

                            <View>
                                <TextInput
                                    keyboardType={'number-pad'}
                                    style={{ backgroundColor: '#FCFCFC', borderRadius: 8, borderColor: '#EEEEEE', borderWidth: 1 }} />
                            </View>
                        </View>

                        <View style={{ marginBottom: 12 }}>

                            <View style={{ marginBottom: 4 }}>
                                <Text style={{ color: '#AAAAAA', fontFamily: 'Kanit', fontSize: 14, }}>Total Price</Text>
                            </View>

                            <View>
                                <Text style={{ color: '#9DC6A7', fontWeight: 'bold', fontSize: 16, fontFamily: 'Kanit' }}>0</Text>
                            </View>
                        </View>

                        <TouchableOpacity
                            onPress={() => setShowModalEdit(false)}
                            style={{ flex: 1, margin: 16, }}>
                            <View style={{ height: 40, justifyContent: 'center', alignItems: 'center', backgroundColor: '#74957C', borderRadius: 24 }}>
                                <Text style={{ color: '#FFFFFF', fontSize: 16, fontWeight: 'bold' }}>EDIT</Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                </View>
            </Modal>

            {show && (<DateTimePicker

                value={date}
                mode={mode}
                display="default"
                onChange={onChange} />
            )}

        </ScrollView >
    )
}